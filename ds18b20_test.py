from ds18b20 import PiDS18B20

hardware_id = '28-0000055d08c4'
mysensor = PiDS18B20(hardware_id)

print 'Sensor ' + mysensor.id + ' shows a reading of ' + str(mysensor.ReadTempF) + 'F'
