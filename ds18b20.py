# Because these temp sensors employ a one-wire bus, this module will assume you
# have identied the hardware identities of each wire on the bus.
# When an instance of a sensor is created, you pass the hardware ID to it.
# For example:
#	sensor1 = '28-0000055d08c4'
#	outdoor_temp_sensor = PiDS18B20(sensor1)
#   current_temp = outdoor_temp_sensor.read_temp()
#
# First append the following line to the end of /boot/config.txt
#    dtoverlay=w1-gpio
#
# To wire up the DS18B20 temp sensor:
# 	Pin 1 goes to ground
#	Pin 2 goes to GPIO 4
#	Pin 3 goes to 3.3V
#	Bridge sensor Pin 2 and Pin 3 with a 4.7k ohm resistor

# Guidance on use of DS18B20 one-wire temp sensor taken from:
# https://pimylifeup.com/raspberry-pi-temperature-sensor/
#
# Note that it takes about 1 sec to pull the reading when read_temp() is called.

import os
import glob
import time

os.system('sudo modprobe w1-gpio')
os.system('sudo modprobe w1-therm')

# All one-wire devices will appear as directories in this location
base_dir = '/sys/bus/w1/devices/'

# Define a class for creating a temp sensor object
class PiDS18B20(object):

	# Constructor
    def __init__(self,hardware_id):
    	self.id = hardware_id

	# Read data values pulled by this sensor object
	def readTempData(self):
		device_folder = base_dir + self.id
		data_file_path = device_folder + '/w1_slave'

		f = open(data_file_path, 'r')
		lines = f.readlines()
		f.close()
		while lines[0].strip()[-3:] != 'YES':
			time.sleep(0.2)
			lines = read_temp_raw()
		equals_pos = lines[1].find('t=')
		if equals_pos != -1:
			temp_string = lines[1][equals_pos+2:]
			temp_c = float(temp_string) / 1000.0
			temp_f = temp_c * 9.0 / 5.0 + 32.0
		return temp_c, temp_f

	(Temp_C, Temp_F) = readTempData(self)
	self.ReadTempC = Temp_C
	self.ReadTempF = Temp_F
